/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxprogram;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
/**
 *
 * @author DELL
 */
public class BoardFrame extends javax.swing.JFrame  {
    private int row;
    private int col;
    JButton btnTables[][] = null;
    Table table;
    Player o;
    Player x;
    

    /**
     * Creates new form BoardFrame
     */
    public BoardFrame() {
        initComponents();
        initTable();
        initGame();
        showTable();
        showTurn();
        hideNewGame();
        btnNewGame.addActionListener(new ActionListener(){
             @Override
        public void actionPerformed(ActionEvent e) {
                newGame();
        }
        }); 
    }

    public void initGame() {
        o = new Player('O');
        x = new Player('X');
        table = new Table(o,x);
    }

    public void initTable() {
        JButton tables[][] = {{btnTable1,btnTable2,btnTable3},
            {btnTable4,btnTable5,btnTable6},
            {btnTable7,btnTable8,btnTable9}};
        btnTables = tables;
        for(int i=0; i<btnTables.length;i++){
            for(int j=0; j<btnTables.length;j++){
                btnTables[i][j].addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String strCom = e.getActionCommand();
                        String coms[] = strCom.split(",");
                        row=Integer.parseInt(coms[0]);
                        col=Integer.parseInt(coms[1]);
                       
                        if(table.setRowCol(row, col)){
                            showTable();
                            if(table.checkWin()){
                                showWin();
                                
                                return;
                            }
                            switchTurn();
                        
                        
                        }
                    }
                });
            }
        }
    }
    
    private void showTable () {
       char data[][] = table.getData();
       for(int i=0; i<btnTables.length;i++){
            for(int j=0; j<btnTables.length;j++){
                btnTables[i][j].setText("" + data[i][j]);
                
            }
    }
     }
     
    private void hideNewGame(){
         btnNewGame.setVisible(false);
     }
     
    private void showNewGame(){
         btnNewGame.setVisible(true);
          showScore();
     }
      
    private void newGame() {
        table = new Table(o,x);
        showTable();
         for(int i=0; i<btnTables.length;i++){
            for(int j=0; j<btnTables.length;j++){
                btnTables[i][j].setEnabled(true);              
            }
    }
    }
    
    private void switchTurn() {
        table.switchPlayer();
        showTurn();
    }

    private void showTurn() {
        lblRowCol.setText("Turn :"+table.getCurrentPlayer().getName());
    }
    
    private void showWin() {
        if(table.getWinner()==null){
            lblRowCol.setText(" Draw");
        }else{
             lblRowCol.setText(table.getCurrentPlayer().getName()+" Winner!!!");
        }
          for(int i=0; i<btnTables.length;i++){
            for(int j=0; j<btnTables.length;j++){
                btnTables[i][j].setEnabled(false);         
            }
    }
          showNewGame();
    }
 
    private void showRowCol() {
        lblRowCol.setText("Row: "+row+"Col: "+col);
    }
    
    private void showScore() {
        xWin.setText("Win: " + x.getWin());
        oWin.setText("Win: " + o.getWin());
        xLose.setText("Lose: " + x.getLose());
        oLose.setText("Lose: " + o.getLose());
        xDraw.setText("Draw: " + x.getDraw());
        oDraw.setText("Draw: " + o.getDraw());

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        btnTable1 = new javax.swing.JButton();
        btnTable2 = new javax.swing.JButton();
        btnTable3 = new javax.swing.JButton();
        btnTable4 = new javax.swing.JButton();
        btnTable5 = new javax.swing.JButton();
        btnTable6 = new javax.swing.JButton();
        btnTable7 = new javax.swing.JButton();
        btnTable8 = new javax.swing.JButton();
        btnTable9 = new javax.swing.JButton();
        lblRowCol = new javax.swing.JLabel();
        btnNewGame = new javax.swing.JButton();
        playerx = new javax.swing.JPanel();
        xDraw = new javax.swing.JLabel();
        xWin = new javax.swing.JLabel();
        xLose = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        playero = new javax.swing.JPanel();
        oDraw = new javax.swing.JLabel();
        oWin = new javax.swing.JLabel();
        oLose = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        jButton1.setText("jButton1");

        jLabel1.setText("jLabel1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnTable1.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 48)); // NOI18N
        btnTable1.setText("-");
        btnTable1.setActionCommand("1,1");
        btnTable1.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable1.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable1.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable2.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 48)); // NOI18N
        btnTable2.setText("-");
        btnTable2.setActionCommand("1,2");
        btnTable2.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable2.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable2.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable3.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 48)); // NOI18N
        btnTable3.setText("-");
        btnTable3.setActionCommand("1,3");
        btnTable3.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable3.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable3.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable4.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 48)); // NOI18N
        btnTable4.setText("-");
        btnTable4.setActionCommand("2,1");
        btnTable4.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable4.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable4.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable5.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 48)); // NOI18N
        btnTable5.setText("-");
        btnTable5.setActionCommand("2,2");
        btnTable5.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable5.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable5.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable6.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 48)); // NOI18N
        btnTable6.setText("-");
        btnTable6.setActionCommand("2,3");
        btnTable6.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable6.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable6.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable7.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 48)); // NOI18N
        btnTable7.setText("-");
        btnTable7.setActionCommand("3,1");
        btnTable7.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable7.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable7.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable8.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 48)); // NOI18N
        btnTable8.setText("-");
        btnTable8.setActionCommand("3,2");
        btnTable8.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable8.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable8.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable9.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 48)); // NOI18N
        btnTable9.setText("-");
        btnTable9.setActionCommand("3,3");
        btnTable9.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable9.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable9.setPreferredSize(new java.awt.Dimension(80, 80));

        lblRowCol.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 24)); // NOI18N
        lblRowCol.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblRowCol.setText("Row: ?,Col: ?");

        btnNewGame.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 24)); // NOI18N
        btnNewGame.setText("New Game");

        playerx.setBackground(new java.awt.Color(224, 187, 224));

        xDraw.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 14)); // NOI18N
        xDraw.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        xWin.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 14)); // NOI18N
        xWin.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        xLose.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 14)); // NOI18N
        xLose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jLabel2.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 18)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Player x");

        javax.swing.GroupLayout playerxLayout = new javax.swing.GroupLayout(playerx);
        playerx.setLayout(playerxLayout);
        playerxLayout.setHorizontalGroup(
            playerxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(playerxLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(playerxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(playerxLayout.createSequentialGroup()
                        .addComponent(xDraw, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(xWin, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xLose, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        playerxLayout.setVerticalGroup(
            playerxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, playerxLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                .addGap(26, 26, 26)
                .addGroup(playerxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(playerxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(xLose, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(xDraw, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(xWin, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24))
        );

        playero.setBackground(new java.awt.Color(224, 187, 224));

        oDraw.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 14)); // NOI18N

        oWin.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 14)); // NOI18N

        oLose.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 14)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 18)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Player o");

        javax.swing.GroupLayout playeroLayout = new javax.swing.GroupLayout(playero);
        playero.setLayout(playeroLayout);
        playeroLayout.setHorizontalGroup(
            playeroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, playeroLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(playeroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(playeroLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(playeroLayout.createSequentialGroup()
                        .addComponent(oDraw, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(oWin, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(oLose, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7))
        );
        playeroLayout.setVerticalGroup(
            playeroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(playeroLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addGroup(playeroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(oDraw, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(playeroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(oLose, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(oWin, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(28, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnTable1, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable2, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable3, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnTable4, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable5, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable6, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnTable7, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable8, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable9, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(playerx, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(playero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(9, 9, 9))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblRowCol, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnNewGame, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(playerx, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(playero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnTable1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnTable4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(4, 4, 4)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnTable7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(lblRowCol, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnNewGame)
                .addGap(33, 33, 33))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BoardFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BoardFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BoardFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BoardFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BoardFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnNewGame;
    private javax.swing.JButton btnTable1;
    private javax.swing.JButton btnTable2;
    private javax.swing.JButton btnTable3;
    private javax.swing.JButton btnTable4;
    private javax.swing.JButton btnTable5;
    private javax.swing.JButton btnTable6;
    private javax.swing.JButton btnTable7;
    private javax.swing.JButton btnTable8;
    private javax.swing.JButton btnTable9;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel lblRowCol;
    private javax.swing.JLabel oDraw;
    private javax.swing.JLabel oLose;
    private javax.swing.JLabel oWin;
    private javax.swing.JPanel playero;
    private javax.swing.JPanel playerx;
    private javax.swing.JLabel xDraw;
    private javax.swing.JLabel xLose;
    private javax.swing.JLabel xWin;
    // End of variables declaration//GEN-END:variables

 
}

  
